package com.example.gcg.controller;


import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/v1" )
public class DemoRestController {

    @PostMapping("/post")
    public String restPost() {

        return "REST POST works!";
    }

    @PutMapping("/put")
    public String restPut() {

        return "REST PUT works!";
    }

    @GetMapping("/get")
    public String restGet() {

        return "REST GET works!";
    }

    @DeleteMapping("/delete")
    public String restDelete() {

        return "REST DELETE works!";
    }

}
